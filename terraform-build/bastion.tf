resource "aws_instance" "bastionbox" {
  count = 1
  ami = "${var.op1-ami}"
  instance_type = "${var.awst2micro}"
  availability_zone = "${var.aws_az1}"
  vpc_security_group_ids = ["${aws_security_group.bastionaccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.bastion_subnet_az1.id}"


  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy1}${var.prefix}-bastion${count.index + 1}
              sudo echo "${var.az1}${var.prefix}-bastion${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.az1}${var.prefix}-bastion${count.index + 1}"
    Owner = "${var.owner}"
    ansibleFilter = "${var.ansibleFilter}" # Must change in ec2.ini
    ansibleNodeType = "bastion"
    ansibleNodeName = "${var.az1}${var.prefix}-bastion${count.index + 1}"
  }
}