resource "aws_vpc" "kube-jh" {
    cidr_block = "${var.vpc_cidr}"
    enable_dns_support = "true"
    enable_dns_hostnames ="true"
      tags {
    Name = "${var.prefix}-kube-vpc"
  }
}


# internet gateway
resource "aws_internet_gateway" "jh-gw" {
  vpc_id = "${aws_vpc.main-jh.id}"
  tags {
    Name = "${var.prefix}-kube-gw"
  }
}


# the Elastic IP in to the network
resource "aws_eip" "eip_in" {
  vpc = true
  instance = "${aws_instance.bastionbox.id}"
}


resource "aws_default_route_table" "jh-route" {
    default_route_table_id = "${aws_vpc.kube-jh.default_route_table_id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.jh-gw.id}"
    }

    tags {
        Name = "${var.prefix}-default"
    }
}


resource "aws_default_network_acl" "main-acl" {
    default_network_acl_id = "${aws_vpc.kube-jh.default_network_acl_id}"
    subnet_ids = ["${aws_subnet.bastion_subnet_az1.id}"]

    ingress {
        protocol = "-1"
        rule_no = 100
        action = "allow"
        cidr_block =  "0.0.0.0/0"
        from_port = 0
        to_port = 0
    }

        egress {
        protocol = "-1"
        rule_no = 100
        action = "allow"
        cidr_block =  "0.0.0.0/0"
        from_port = 0
        to_port = 0
    }

    tags {
        Name = "${var.prefix}-default-acl"
    }