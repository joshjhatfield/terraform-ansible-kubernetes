resource "aws_instance" "az1node" {
  ami = "${var.op1-ami}"
  instance_type = "${var.awst2micro}"
  availability_zone = "${var.aws_az1}"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.node_subnet-az1.id}"
  
  root_block_device = {
  volume_type = "standard"
  volume_size = "30"
 } 

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy1}${var.prefix}-node${count.index + 1}
              sudo echo "${var.az1}${var.prefix}-node${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.az1}${var.prefix}-node${count.index + 1}"
    Owner = "${var.owner}"
    ansibleFilter = "${var.ansibleFilter}" # Must change in ec2.ini
    ansibleNodeType = "node"
    ansibleNodeName = "${var.az1}${var.prefix}-node${count.index + 1}"
  }
}



resource "aws_instance" "az2node" {
  ami = "${var.op1-ami}"
  instance_type = "${var.awst2micro}"
  availability_zone = "${var.aws_az2}"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.node_subnet-az2.id}"
  
  root_block_device = {
  volume_type = "standard"
  volume_size = "30"
 } 

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy1}${var.prefix}-node${count.index + 1}
              sudo echo "${var.az2}${var.prefix}-node${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.az2}${var.prefix}-node${count.index + 1}"
    Owner = "${var.owner}"
    ansibleFilter = "${var.ansibleFilter}" # Must change in ec2.ini
    ansibleNodeType = "node"
    ansibleNodeName = "${var.az2}${var.prefix}-node${count.index + 1}"
  }
}


resource "aws_instance" "az3node" {
  ami = "${var.op1-ami}"
  instance_type = "${var.awst2micro}"
  availability_zone = "${var.aws_az3}"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.node_subnet-az3.id}"
  
  root_block_device = {
  volume_type = "standard"
  volume_size = "30"
 } 

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy1}${var.prefix}-node${count.index + 1}
              sudo echo "${var.az3}${var.prefix}-node${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.az3}${var.prefix}-node${count.index + 1}"
    Owner = "${var.owner}"
    ansibleFilter = "${var.ansibleFilter}" # Must change in ec2.ini
    ansibleNodeType = "node"
    ansibleNodeName = "${var.az3}${var.prefix}-node${count.index + 1}"
  }
}