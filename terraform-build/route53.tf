resource "aws_route53_zone" "kcluster" {
  name = "${var.privdns}"
  vpc_id = "${aws_vpc.kube-jh.id}"
  force_destroy = "true"
  tags {
  	Name = "${var.privdns}"
  }
}

# bastion server record
resource "aws_route53_record" "sy1bastionrecord" {
 zone_id = "${aws_route53_zone.kcluster.zone_id}"
 name = "${var.az1}${var.prefix}-bastion${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.bastionbox.private_ip}"]
}

# node records
resource "aws_route53_record" "az1noderecord" {
 zone_id = "${aws_route53_zone.kcluster.zone_id}"
 name = "${var.az1}${var.prefix}-node${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1node.private_ip}"]
}
resource "aws_route53_record" "az2noderecord" {
 zone_id = "${aws_route53_zone.kcluster.zone_id}"
 name = "${var.az2}${var.prefix}-node${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az2node.private_ip}"]
}
resource "aws_route53_record" "az3noderecord" {
 zone_id = "${aws_route53_zone.kcluster.zone_id}"
 name = "${var.az3}${var.prefix}-node${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az3node.private_ip}"]
}