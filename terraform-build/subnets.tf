# Subnet config


# Bastion server subnet

resource "aws_subnet" "bastion_subnet_az1" {
    vpc_id = "${aws_vpc.kube-jh.id}"
    cidr_block = "${var.bastion_az1}"
    availability_zone = "${var.aws_az1}"
    map_public_ip_on_launch = true
    depends_on = ["aws_internet_gateway.jh-gw"]
    tags {
        Name = "${var.prefix}-Bastion-${var.az1}"
    }
}

# Node subnets

resource "aws_subnet" "node_subnet-az1" {
    vpc_id = "${aws_vpc.kube-jh.id}"
    cidr_block = "${var.node_az1}"
    availability_zone = "${var.aws_az1}"
    depends_on = ["aws_internet_gateway.jh-gw"]
    tags {
        Name = "${var.prefix}-node-${var.az1}"
    }
}

resource "aws_subnet" "node_subnet-az2" {
    vpc_id = "${aws_vpc.kube-jh.id}"
    cidr_block = "${var.node_az2}"
    availability_zone = "${var.aws_az2}"
    depends_on = ["aws_internet_gateway.jh-gw"]

    tags {
        Name = "${var.prefix}-node-${var.az2}"
    }
}


resource "aws_subnet" "node_subnet-az3" {
    vpc_id = "${aws_vpc.kube-jh.id}"
    cidr_block = "${var.node_az3}"
    availability_zone = "${var.aws_az3}"
    depends_on = ["aws_internet_gateway.jh-gw"]

    tags {
        Name = "${var.prefix}-node-${var.az3}"
    }
}


# Master subnets

resource "aws_subnet" "master_subnet-az1" {
    vpc_id = "${aws_vpc.kube-jh.id}"
    cidr_block = "${var.master_az1}"
    availability_zone = "${var.aws_az1}"
    depends_on = ["aws_internet_gateway.jh-gw"]

    tags {
        Name = "${var.prefix}-master-${var.az1}"
    }
}


# Adm server subnet

resource "aws_subnet" "adm_subnet-az1" {
    vpc_id = "${aws_vpc.kube-jh.id}"
    cidr_block = "${var.adm_az1}"
    availability_zone = "${var.aws_az1}"
    depends_on = ["aws_internet_gateway.jh-gw"]

    tags {
        Name = "${var.prefix}-adm-${var.az1}"
    }
}