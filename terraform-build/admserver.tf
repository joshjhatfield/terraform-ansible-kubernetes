resource "aws_instance" "az1adm" {
  ami = "${var.op1-ami}"
  instance_type = "${var.awsm4large}"
  availability_zone = "${var.aws_az1}"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.adm_subnet-az1.id}"
  
  root_block_device = {
  volume_type = "standard"
  volume_size = "250"
 } 

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.az1}${var.prefix}-adm${count.index + 1}
              sudo echo "${var.az1}${var.prefix}-adm${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.az1}${var.prefix}-adm${count.index + 1}"
    Owner = "${var.owner}"
    ansibleFilter = "${var.ansibleFilter}" # Must change in ec2.ini
    ansibleNodeType = "adm"
    ansibleNodeName = "${var.az1}${var.prefix}-adm${count.index + 1}"
  }
}
