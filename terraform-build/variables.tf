# Generic Vars file for the build. 

variable "aws_access_key" {}
variable "aws_secret_key" {}

#######
# Global vars
#######


# The Global server prefix
variable "prefix" {
  default ="dev"
  description = "the global prefix for this build"
}


# naming servers by AZ
variable "az1" {
  default ="az1"
  description = "Availability zone 1 prefix"
}

variable "az2" {
  default ="az2"
  description = "Availability zone 2 prefix"
}

variable "az3" {
  default ="az3"
  description = "Availability zone 3 prefix"
}


# The private DNS record
variable "privdns" {
  default = "dev.kubeenv.internal"
  description = "The global private dns record for route53"
}

# AWS region
variable "aws_region" {
    description = "aws region to use"
    default = "ap-southeast-2"
}


# AWS Availability zones
variable "aws_az1" {
    description = "az1 Availability zones"
    default = "ap-southeast-2a"
}

variable "aws_az2" {
    description = "az1 Availability zones"
    default = "ap-southeast-2b"
}

variable "aws_az3" {
    description = "az3 Availability zones"
    default = "ap-southeast-2c"
}

# IAM key name
variable "iamkey" {
  default ="puppet"
  description = "name of ssh key being used, create a key resource otherwise."
}

#####
# AMI Config
#####

# 1st option
variable "op1-ami" {
  description= " Ubuntu 16.04 AMI for bastion"
  default= "ami-4d3b062e"
}

# Machine sizes
variable "awst2micro" {
  description= "t2.micro AMI size"
  default= "t2.micro"
}

variable "awsm4large" {
  description= "m4.large instance size 2xcpu, 8GB ram"
  default= "m4.large"
}


#####
# Ansible vars
#####

variable "ansibleFilter" {
	description= "default andible filter defines in ec2.ini"
	default= "jhkubeenv"
}

variable "owner" {
	description= "server owner tag"
	default= "kubejh"
}

#####
# VPC And networking
#####

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.25.0.0/16"
}

# bastion
variable "bastion_az1" {
    description = "CIDR for az1 internal bastion subnet"
    default = "10.25.5.0/24"
}


# node subnets
variable "node_az1" {
    description = "CIDR for az1 internal node subnet"
    default = "10.25.10.0/24"
}


variable "node_az2" {
    description = "CIDR for az2 internal node subnet"
    default = "10.25.11.0/24"
}


variable "node_az3" {
    description = "CIDR for az3 internal node subnet"
    default = "10.25.12.0/24"
}


# masters
variable "master_az1" {
    description = "CIDR for az1 internal master subnet"
    default = "10.25.20.0/24"
}


# adm/utility server
variable "adm_az1" {
  description = "the subnet for the puppet, ELK and ansible servers"
  default = "10.25.40.0/24"
}

variable "adm_az2" {
  description = "the subnet for the puppet, ELK and ansible servers"
  default = "10.25.41.0/24"
}