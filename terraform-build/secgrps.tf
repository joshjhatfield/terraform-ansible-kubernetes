# internal SG
resource "aws_security_group" "serveraccess" {
  name = "${var.prefix}-access-group"
  description ="Access to the servers for SSH and repos"
  vpc_id = "${aws_vpc.kube-jh.id}"
  # remove this rule post install to harden environment.
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["10.25.0.0/16"]
  }
   egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# Bastion SG
resource "aws_security_group" "bastionaccess" {
  name = "${var.prefix}-bastion-group"
  description ="Bastion in and out connections"
  vpc_id = "${aws_vpc.kube-jh.id}"
  # remove this rule post install to harden environment.
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }
}