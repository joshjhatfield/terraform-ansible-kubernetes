#!/bin/bash
# A script to set up an adm server. 

# elastic setup
elastic ()
{
	sudo mkdir /tmp/setupdir
	# install basic utils
	sudo apt-get update
	sudo apt-get -y install openjdk-7-jre
	# Download and install elastic for ubuntu servers
	cd /tmp/setupdir
	wget https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/deb/elasticsearch/2.4.1/elasticsearch-2.4.1.deb
	sudo dpkg -i elasticsearch-2.4.1.deb
	service elasticsearch start
}


ansible ()
{
	sudo apt-get -y install software-properties-common
	sudo apt-add-repository -y ppa:ansible/ansible
	sudo apt-get update
	sudo apt-get -y install git ansible
}

terraform ()
{
	cd /tmp/setupdir
	sudo apt-get -y install unzip
	wget https://releases.hashicorp.com/terraform/0.7.7/terraform_0.7.7_linux_amd64.zip
	unzip terraform_0.7.7_linux_amd64.zip
	sudo mv terraform /usr/local/bin/
}



# run condition
if [[ $1 == run ]]; then
	elastic
	sleep 10s
	ansible
	sleep 10s
	terraform
	sleep 10s
fi